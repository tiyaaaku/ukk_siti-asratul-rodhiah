<?php
// Konfigurasi koneksi ke database
$host = 'localhost'; // Ganti dengan host Anda
$user = 'root'; // Ganti dengan username Anda
$password = ''; // Ganti dengan password Anda
$database = 'ppdb'; // Ganti dengan nama database Anda

// Membuat koneksi ke database
$koneksi = new mysqli($host, $user, $password, $database);

// Memeriksa koneksi
if ($koneksi->connect_error) {
    die("Koneksi database gagal: " . $koneksi->connect_error);
}

// Menangkap data yang dikirimkan dari formulir
$nama = $_POST['nama'];
$jenis_kelamin = $_POST['jenis_kelamin'];
$tempat_lahir = $_POST['tempat_lahir'];
$tanggal_lahir = $_POST['tanggal_lahir'];
$alamat = $_POST['alamat'];                 
$telepon = $_POST['telepon'];
$email = $_POST['email'];
$sekolah_asal = $_POST['sekolah_asal'];
$nilai_rapor = $_POST['nilai_rapor'];
$jurusan_pilihan = $_POST['jurusan_pilihan'];
$nisn = $_POST['nisn'];

// Menyiapkan query untuk menyimpan data pendaftaran ke dalam tabel pendaftaran
$query = "INSERT INTO pendaftaran (nama, jenis_kelamin, tempat_lahir, tanggal_lahir, alamat, telepon, email, sekolah_asal, nilai_rapor, jurusan_pilihan, nisn) 
          VALUES ('$nama', '$jenis_kelamin', '$tempat_lahir', '$tanggal_lahir', '$alamat', '$telepon', '$email', '$sekolah_asal', '$nilai_rapor', '$jurusan_pilihan', '$nisn')";

// Menjalankan query
if ($koneksi->query($query) === TRUE) {
    echo "Pendaftaran berhasil!";
} else {
    echo "Error: " . $query . "<br>" . $koneksi->error;
}

// Menutup koneksi
$koneksi->close();
?>
<?php
// Proses formulir disini
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Proses data formulir
    // Misalnya, menyimpan data ke database

    // Redirect ke halaman data pendaftaran setelah proses selesai
    header("Location: data_pendaftaran.php");
    exit();
}
?>

