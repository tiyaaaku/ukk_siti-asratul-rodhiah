<?php
// Memeriksa apakah parameter id telah diberikan melalui URL
if (isset($_GET['id'])) {
    // Konfigurasi koneksi ke database
    $host = 'localhost'; // Ganti dengan host Anda
    $user = 'root'; // Ganti dengan username Anda
    $password = ''; // Ganti dengan password Anda
    $database = 'ppdb'; // Ganti dengan nama database Anda

    // Membuat koneksi ke database
    $koneksi = new mysqli($host, $user, $password, $database);

    // Memeriksa koneksi
    if ($koneksi->connect_error) {
        die("Koneksi database gagal: " . $koneksi->connect_error);
    }

    // Mendapatkan nilai id dari parameter URL
    $id = $_GET['id'];

    // Menyiapkan query untuk menghapus data pendaftaran
    $query = "DELETE FROM pendaftaran WHERE id=$id";

    // Menjalankan query
    if ($koneksi->query($query) === TRUE) {
        // Jika penghapusan berhasil, arahkan kembali ke halaman data pendaftaran
        header("Location: data_pendaftaran.php");
        exit;
    } else {
        echo "Error: " . $query . "<br>" . $koneksi->error;
    }

    // Menutup koneksi
    $koneksi->close();
} else {
    echo "ID tidak ditemukan.";
}
?>
