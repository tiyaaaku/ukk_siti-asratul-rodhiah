<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Pendaftaran PPDB</title>
    <!-- Bootstrap CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body {
            background-image: url('apasih.jpg'); /* Ganti 'gambar-background.jpg' dengan URL atau path file gambar latar belakang Anda */
            background-size: cover;
            background-repeat: no-repeat;
            background-attachment: fixed;
        }
    </style>
</head>
<body>
    <div class="container">
        <h2 class="mt-4">Data Siswa Yang Akan Mengikuti Seleksi</h2>
        <a href="tambah.php" class="btn btn-primary mb-3">Tambah Data</a>
        <table class="table">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Jenis Kelamin</th>
                    <th>Tempat Lahir</th>
                    <th>Tanggal Lahir</th>
                    <th>Alamat</th>
                    <th>Telepon</th>
                    <th>Email</th>
                    <th>Sekolah Asal</th>
                    <th>Nilai Rapor</th>
                    <th>Jurusan Pilihan</th>
                    <th>NISN</th>
                    <th>Tanggal Pendaftaran</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php
                // Konfigurasi koneksi ke database
                $host = 'localhost'; // Ganti dengan host Anda
                $user = 'root'; // Ganti dengan username Anda
                $password = ''; // Ganti dengan password Anda
                $database = 'ppdb'; // Ganti dengan nama database Anda

                // Membuat koneksi ke database
                $koneksi = new mysqli($host, $user, $password, $database);

                // Memeriksa koneksi
                if ($koneksi->connect_error) {
                    die("Koneksi database gagal: " . $koneksi->connect_error);
                }

                // Menyiapkan query untuk mengambil data pendaftaran dari tabel pendaftaran
                $query = "SELECT * FROM pendaftaran";
                $result = $koneksi->query($query);

                // Menampilkan data dalam format tabel
                if ($result->num_rows > 0) {
                    $no = 1;
                    while($row = $result->fetch_assoc()) {
                        echo "<tr>";
                        echo "<td>" . $no++ . "</td>";
                        echo "<td>" . $row['nama'] . "</td>";
                        echo "<td>" . $row['jenis_kelamin'] . "</td>";
                        echo "<td>" . $row['tempat_lahir'] . "</td>";
                        echo "<td>" . $row['tanggal_lahir'] . "</td>";
                        echo "<td>" . $row['alamat'] . "</td>";
                        echo "<td>" . $row['telepon'] . "</td>";
                        echo "<td>" . $row['email'] . "</td>";
                        echo "<td>" . $row['sekolah_asal'] . "</td>";
                        echo "<td>" . $row['nilai_rapor'] . "</td>";
                        echo "<td>" . $row['jurusan_pilihan'] . "</td>";
                        echo "<td>" . $row['nisn'] . "</td>";
                        echo "<td>" . $row['tanggal_pendaftaran'] . "</td>";
                        echo "<td>";
                        echo "<a href='ubah_pendaftaran.php?id=" . $row['id'] . "' class='btn btn-warning btn-sm mr-1'>Ubah</a>";
                        echo "<a href='hapus_pendaftaran.php?id=" . $row['id'] . "' class='btn btn-danger btn-sm'>Hapus</a>";
                        echo "</td>";
                        echo "</tr>";
                    }
                } else {
                    echo "<tr><td colspan='14'>Tidak ada data pendaftaran.</td></tr>";
                }
                // Menutup koneksi
                $koneksi->close();
                ?>
                
            </body>
        </table>
    </div>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
