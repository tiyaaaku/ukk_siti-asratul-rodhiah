<?php
// Memeriksa apakah parameter id telah diberikan melalui URL
if (isset($_GET['id'])) {
    // Konfigurasi koneksi ke database
    $host = 'localhost'; // Ganti dengan host Anda
    $user = 'root'; // Ganti dengan username Anda
    $password = ''; // Ganti dengan password Anda
    $database = 'ppdb'; // Ganti dengan nama database Anda

    // Membuat koneksi ke database
    $koneksi = new mysqli($host, $user, $password, $database);

    // Memeriksa koneksi
    if ($koneksi->connect_error) {
        die("Koneksi database gagal: " . $koneksi->connect_error);
    }

    // Mendapatkan nilai id dari parameter URL
    $id = $_GET['id'];

    // Menyiapkan query untuk mengambil data pendaftaran berdasarkan ID
    $query_select = "SELECT * FROM pendaftaran WHERE id=$id";
    $result = $koneksi->query($query_select);

    // Memeriksa apakah data ditemukan
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Konfirmasi Penghapusan Data</title>
    <!-- Bootstrap CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <div class="container">
        <h2 class="mt-4">Konfirmasi Penghapusan Data</h2>
        <p>Apakah Anda yakin ingin menghapus data pendaftaran berikut?</p>
        <p>Nama: <?php echo $row['nama']; ?></p>
        <!-- Tambahkan detail lainnya sesuai kebutuhan -->

        <form action="proses_hapus_pendaftaran.php?id=<?php echo $id; ?>" method="post">
            <button type="submit" class="btn btn-danger">Ya, Hapus Data</button>
            <a href="data_pendaftaran.php" class="btn btn-secondary">Batalkan</a>
        </form>
    </div>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
<?php
    } else {
        echo "Data tidak ditemukan.";
    }

    // Menutup koneksi
    $koneksi->close();
} else {
    echo "ID tidak ditemukan.";
}
?>
