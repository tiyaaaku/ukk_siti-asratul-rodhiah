<?php
// Koneksi ke database
$host = 'localhost'; // Ganti dengan host Anda
$user = 'root'; // Ganti dengan username Anda
$password = ''; // Ganti dengan password Anda
$database = 'ppdb'; // Ganti dengan nama database Anda

// Membuat koneksi ke database
$koneksi = new mysqli($host, $user, $password, $database);

// Memeriksa koneksi
if ($koneksi->connect_error) {
    die("Koneksi database gagal: " . $koneksi->connect_error);
}

// Memeriksa apakah parameter ID dikirim melalui URL
if (isset($_GET['id'])) {
    $id = $_GET['id'];

    // Menyiapkan query untuk mengambil data pendaftaran berdasarkan ID
    $query = "SELECT * FROM pendaftaran WHERE id = $id";
    $result = $koneksi->query($query);

    // Memeriksa apakah data ditemukan
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ubah Data Pendaftaran</title>
    <!-- Bootstrap CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <div class="container">
        <h2 class="mt-4">Ubah Data Pendaftaran</h2>
        <form action="proses_ubah_pendaftaran.php" method="post">
            <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
            <div class="form-group">
                <label for="nama">Nama:</label>
                <input type="text" class="form-control" id="nama" name="nama" value="<?php echo $row['nama']; ?>" required>
            </div>

            <div class="form-group">
                <label for="jenis_kelamin">Jenis Kelamin:</label><br>
                <input type="radio" name="jenis_kelamin" id="laki-laki" value="Laki-laki" <?php if($row['jenis_kelamin'] == "Laki-laki") echo "checked"; ?> required>
                <label for="laki-laki">Laki-laki</label>
                <input type="radio" name="jenis_kelamin" id="perempuan" value="Perempuan" <?php if($row['jenis_kelamin'] == "Perempuan") echo "checked"; ?>>
                <label for="perempuan">Perempuan</label>
            </div>

            <div class="form-group">
                <label for="tempat_lahir">Tempat Lahir:</label>
                <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" value="<?php echo $row['tempat_lahir']; ?>" required>
            </div> 

            <div class="form-group">
                <label for="tanggal_lahir">Tanggal Lahir:</label>
                <input type="date" class="form-control" id="tanggal_lahir" name="tanggal_lahir" value="<?php echo $row['tanggal_lahir']; ?>" required>
            </div>

            <div class="form-group">
                <label for="alamat">Alamat:</label>
                <textarea class="form-control" id="alamat" name="alamat" rows="4" required><?php echo $row['alamat']; ?></textarea>
            </div>

            <div class="form-group">
                <label for="telepon">Telepon:</label>
                <input type="tel" class="form-control" id="telepon" name="telepon" value="<?php echo $row['telepon']; ?>" required>
            </div>

            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" class="form-control" id="email" name="email" value="<?php echo $row['email']; ?>" required>
            </div>

            <div class="form-group">
                <label for="sekolah_asal">Sekolah Asal:</label>
                <input type="text" class="form-control" id="sekolah_asal" name="sekolah_asal" value="<?php echo $row['sekolah_asal']; ?>" required>
            </div>

            <div class="form-group">
                <label for="nilai_rapor">Nilai Rapor:</label>
                <input type="number" class="form-control" id="nilai_rapor" name="nilai_rapor" min="0" max="100" step="0.01" value="<?php echo $row['nilai_rapor']; ?>" required>
            </div>

            <div class="form-group">
                <label for="jurusan_pilihan">Jurusan Pilihan:</label>
                <select class="form-control" id="jurusan_pilihan" name="jurusan_pilihan" required>
                    <option value="">Pilih Jurusan</option>
                    <option value="RPL" <?php if($row['jurusan_pilihan'] == "RPL") echo "selected"; ?>>RPL</option>
                    <option value="Multimedia" <?php if($row['jurusan_pilihan'] == "Multimedia") echo "selected"; ?>>Multimedia</option>
                    <option value="PPLG" <?php if($row['jurusan_pilihan'] == "PPLG") echo "selected"; ?>>PPLG</option>
                    <option value="DKV" <?php if($row['jurusan_pilihan'] == "DKV") echo "selected"; ?>>DKV</option>
                </select>
            </div>

            <div class="form-group">
                <label for="nisn">NISN:</label>
                <input type="text" class="form-control" id="nisn" name="nisn" value="<?php echo $row['nisn']; ?>" required>
            </div>

            <div class="form-group">
                <label for="tanggal_pendaftaran">Tanggal Pendaftaran:</label>
                <input type="date" class="form-control" id="tanggal_pendaftaran" name="tanggal_pendaftaran" value="<?php echo $row['tanggal_pendaftaran']; ?>" required>
            </div>

            <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
        </form>

    </div>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
<?php
    } else {
        echo "Data tidak ditemukan.";
    }
} else {
    echo "ID tidak ditemukan dalam URL.";
}


// Menutup koneksi
$koneksi->close();
?>


