
<?php
// Koneksi ke database
$host = 'localhost'; // Ganti dengan host Anda
$user = 'root'; // Ganti dengan username Anda
$password = ''; // Ganti dengan password Anda
$database = 'ppdb'; // Ganti dengan nama database Anda

// Membuat koneksi ke database
$koneksi = new mysqli($host, $user, $password, $database);

// Memeriksa koneksi
if ($koneksi->connect_error) {
    die("Koneksi database gagal: " . $koneksi->connect_error);
}

// Memeriksa apakah data yang diperlukan dikirim melalui formulir
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['id'])) {
    // Mendapatkan data dari formulir
    $id = $_POST['id'];
    $nama = $_POST['nama'];
    $jenis_kelamin = $_POST['jenis_kelamin'];
    $tempat_lahir = $_POST['tempat_lahir'];
    $tanggal_lahir = $_POST['tanggal_lahir'];
    $alamat = $_POST['alamat'];
    $telepon = $_POST['telepon'];
    $email = $_POST['email'];
    $sekolah_asal = $_POST['sekolah_asal'];
    $nilai_rapor = $_POST['nilai_rapor'];
    $jurusan_pilihan = $_POST['jurusan_pilihan'];
    $nisn = $_POST['nisn'];
    $tanggal_pendaftaran = $_POST['tanggal_pendaftaran'];

    // Menyiapkan query untuk memperbarui data pendaftaran
    $query = "UPDATE pendaftaran SET 
              nama='$nama', 
              jenis_kelamin='$jenis_kelamin', 
              tempat_lahir='$tempat_lahir', 
              tanggal_lahir='$tanggal_lahir',
              alamat='$alamat',
              telepon='$telepon',
              email='$email',
              sekolah_asal='$sekolah_asal',
              nilai_rapor='$nilai_rapor',
              jurusan_pilihan='$jurusan_pilihan',
              nisn='$nisn',
              tanggal_pendaftaran='$tanggal_pendaftaran' 
              WHERE id=$id";

    // Melakukan pembaruan data
    if ($koneksi->query($query) === TRUE) {
        echo "Data berhasil diperbarui.";
    } else {
        // Jika terjadi kesalahan saat menjalankan query
        echo "Error: " . $query . "<br>" . $koneksi->error;
    }
} else {
    // Jika tidak ada data yang dikirimkan melalui formulir atau ID tidak ditemukan
    echo "Tidak ada data yang dikirimkan atau ID tidak ditemukan.";
}

// Menutup koneksi
$koneksi->close();
?>
<?php
// Kode untuk melakukan proses ubah data

// Setelah proses ubah selesai, arahkan pengguna ke halaman data pendaftaran
header("Location: data_pendaftaran.php");
exit; // Pastikan untuk menghentikan eksekusi skrip setelah mengarahkan pengguna
?>

